// **
// * Задание 1
// *
// * 1. Реализуем приложение Список дел с возможностью добавить новый элемент в список из инпута по нажатию кнопки "Добавить";
// * 2. При нажатии на галочку текст итема становится зачеркнутым, при отжатии снова обычным.
// * 3. Реализовать удаление элемента по нажатию крестика;
// * 4. Реализовать возможность редактирования элемента:
//     * - При нажатии на текст элемента он отображается в инпуте, название кнопки меняется на "Обновить";
// * - После изменения названия и нажатия на "Обновить" текст элемента изменяется в списке, инпут сбрасывается, кнопка переименовывается обратно в "Добавить".;
// * - Предусмотреть возможность отменить редактирование (крестик в конце инпута), при нажатии на который редактирование прекращается: инпут очищается, кнопка вновь имеет заголовок "Добавить".
// * - Редактируемый элемент подсвечивается как активный.
// * 5*. Сохрание текущего состояния (local storage).
// */

// Нахожу все элементы

const inputEl = document.querySelector('.js-input');
const liValue = document.querySelector('.js-item-value');
const btnCancel = document.querySelector('.js-cancel');
const btnEl = document.querySelector('.js-btn');
const btnAddEl = document.querySelector('.js-btn-add');

// Добавление новой задачи и редактирование

document.querySelector('.js-btn').addEventListener('click', event => {
    if (event.target.classList.contains('btnUpdate')) {  // РЕДАКТИРОВАНИЕ
        const editedElem = document.querySelector('.editText');  // нашла редактируемый элемент
        const editedElemItemValue = editedElem.querySelector('.js-item-value'); // нашла span куда текст записываем
        editedElemItemValue.innerText =inputEl.value // переписываем текст
        btnCancel.classList.add('hidden'); // скрывает крестик для отмены редактирования
        btnEl.textContent = 'Добавить'; // меняем текст кнопки добавить-обновить
        btnEl.classList.remove('btnUpdate'); // меняем вид кнопки добавить-обновить
        inputEl.value = ''; // обнуляет инпут
        document.querySelectorAll('li').forEach(itemEl=> itemEl.classList.remove('editText')) // снимаем выделение цветом на ранее выделенных
    }
    if (event.target.classList.contains('js-btn-add')) { // ДОБАВЛЕНИЕ
        const newLiEl = createLiEl(inputEl.value || 'Пустая задача');
        document.querySelector('.js-group').append(newLiEl);
        inputEl.value = '';
    }
});

function createLiEl(text) {
    const liEl = document.createElement('li');
    liEl.classList.add('list-group-item');
    liEl.classList.add('js-item');

    const formGroupEl = document.createElement('div');
    formGroupEl.classList.add('form-group');

    const checkboxEl = document.createElement('input');
    checkboxEl.classList.add('form-check-input');
    checkboxEl.classList.add('js-checkbox');
    checkboxEl.setAttribute('type', 'checkbox');

    const spanEl = document.createElement('span');
    spanEl.classList.add('js-item-value');
    spanEl.textContent = text;

    const removeEl = document.createElement('div');
    removeEl.classList.add('remove', 'js-remove');
    removeEl.textContent = 'X';

    formGroupEl.append(checkboxEl);
    formGroupEl.append(spanEl);

    liEl.append(formGroupEl);
    liEl.append(removeEl);

    // // для новых элементов
    // checkboxEl.addEventListener('click', event => {
    //     const parent = checkboxEl.parentNode;
    //     parent.querySelector('.js-item-value').classList.toggle('done');
    // });

    return liEl;

}

// const checkboxEl = document.querySelector('.js-checkbox');
//
// checkboxEl.addEventListener('click', event => {
//     const parent = checkboxEl.parentNode;
//     parent.querySelector('.js-item-value').classList.toggle('done');
// });

// для существующих элементов списка на момент загруки стр
// const checkboxList = document.querySelectorAll('.js-checkbox');
//
// for (let i = 0; i < checkboxList.length; i++) {
//     const checkboxItem = checkboxList.item(i);
//     checkboxItem.addEventListener('click', event => {
//         const parent = checkboxItem.parentNode;
//         parent.querySelector('.js-item-value').classList.toggle('done');
//     });
// }

document.querySelector('.js-group').addEventListener('click', event => {
    // 2. При нажатии на галочку текст итема становится зачеркнутым, при отжатии снова обычным.
    if (event.target.classList.contains('js-checkbox')) {
        const parentNode = event.target.parentNode;
        parentNode.querySelector('.js-item-value').classList.toggle('done');
    }
    // 3. Реализовать удаление элемента по нажатию крестика;
    if (event.target.classList.contains('js-remove')) {
        const parent = event.target.parentNode;
        parent.remove();
    }
});

//-----------------------------------------------------------------------------------------------
//4. Реализовать возможность редактирования элемента:


document.querySelector('.js-group').addEventListener('click', event => {
        if (event.target.classList.contains('js-item-value')) {
            document.querySelectorAll('li').forEach(itemEl=> itemEl.classList.remove('editText')) // снимаем выделение цветом на ранее выделенных пунктах
            inputEl.value = event.target.textContent; // показывает мне содержимое  тыкнутого пункта
            event.target.parentNode.parentNode.classList.add('editText'); // подсвечивает элемент
            btnEl.textContent = "Обновить"; // меняет название кнопки
            btnEl.classList.add('btnUpdate'); // меняет вид кнопки
            btnEl.classList.remove('js-btn-add'); // ПОЧЕМУ НЕ УДАЛЯЕТ ФУНКЦИОНАЛ КНОПКИ ПО КЛАССУ?
            btnCancel.classList.remove('hidden'); // показывает крестик для отмены редактирования
        }
    }
);
// * - Предусмотреть возможность отменить редактирование (крестик в конце инпута), при нажатии на который редактирование прекращается: инпут очищается, кнопка вновь имеет заголовок "Добавить".
btnCancel.addEventListener('click', event => {
    document.querySelectorAll('li').forEach(itemEl=> itemEl.classList.remove('editText')) // снимаем выделение цветом на ранее выделенных пунктах
    btnEl.textContent = 'Добавить'; // меняем текст кнопки добавить-обновить
    btnEl.classList.remove('btnUpdate'); // меняем вид кнопки добавить-обновить
    btnEl.classList.add('js-btn-add');//добавляем функционал добавления в список нового объекта
    inputEl.value = ''; // обнуляет инпут
    btnCancel.classList.add('hidden'); // скрывает крестик для отмены редактирования
});

